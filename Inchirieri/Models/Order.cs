﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inchirieri.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public int CarId { get; set; }
        public string UserId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ReturnedDate { get; set; }
        public int TotalPrice { get; set; }
        public string CreatedAt { get; set; }
        public bool IsReturned { get; set; }
    }
}