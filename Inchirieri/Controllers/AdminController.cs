﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Inchirieri.Models;

namespace Inchirieri.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AdminController : Controller
    {

        public SqlConnection DbConnect()
        {
            string connectionString = @"Data Source=DESKTOP-JV89SA9\SQLEXPRESS;Initial Catalog=Inchirieri;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);

            return conn;
        }

        public SqlDataReader ExecuteQuery(string cmd, SqlConnection conn)
        {
            SqlCommand sqlCmd = new SqlCommand(cmd, conn);
            SqlDataReader reader = sqlCmd.ExecuteReader();

            return reader;
        }

        public List<User> GetUsers()
        {
            List<User> users = new List<User>();

            SqlConnection conn = DbConnect();
            conn.Open();

            SqlDataReader reader = ExecuteQuery("Select AspNetUsers.id, AspNetUsers.Email, AspNetUsers.UserName, AspNetRoles.Name from AspNetUsers, AspNetUserRoles, AspNetRoles where AspNetUsers.id = AspNetUserRoles.UserId And AspNetUserRoles.RoleId = AspNetRoles.Id", conn);

            while (reader.Read())
            {
                User usr = new User();
                usr.Id = reader["id"].ToString();
                usr.Name = reader["UserName"].ToString();
                usr.Email = reader["Email"].ToString();
                usr.Role = reader["Name"].ToString();

                users.Add(usr);
            }

            return users;
        }

        public List<Order> GetOrders()
        {
            List<Order> orders = new List<Order>();

            SqlConnection conn = DbConnect();
            conn.Open();

            SqlDataReader reader = ExecuteQuery("select Rentals.order_id, Rentals.order_carid, AspNetUsers.Email, Rentals.order_startedAt, Rentals.order_endedAt, Rentals.order_totalPrice, Rentals.createdAt, Rentals.order_returnedAt from Rentals, AspNetUsers where Rentals.order_custId = AspNetUsers.Id", conn);

            while(reader.Read())
            {
                Order ord = new Order();
                ord.OrderId = int.Parse(reader["order_id"].ToString());
                ord.CarId = int.Parse(reader["order_carid"].ToString());
                ord.UserId = reader["Email"].ToString();
                ord.StartDate = reader["order_startedAt"].ToString();
                ord.EndDate = reader["order_endedAt"].ToString();
                ord.TotalPrice = int.Parse(reader["order_totalPrice"].ToString());
                ord.CreatedAt = reader["createdAt"].ToString();

                if ( DBNull.Value.Equals(reader["order_returnedAt"]))
                {
                    ord.IsReturned = false;
                    ord.ReturnedDate = "-";
                }
                else
                {
                    ord.IsReturned = true;
                    ord.ReturnedDate = reader["order_returnedAt"].ToString();
                }

                orders.Add(ord);
            }
            return orders;
        }


        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.Users = GetUsers();
            ViewBag.Orders = GetOrders();
            return View();
        }

        [Route("admin/order/{orderId}")]
        public ActionResult ReturnOrder(int orderId)
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            string sqlCmd = "Update Rentals Set order_returnedAt = SYSDATETIME(), updatedAt = SYSDATETIME() where order_id=" + orderId;
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.ExecuteNonQuery();

            return RedirectToAction("Index");
        }

        public Dictionary<string, string> GetRoles()
        {
            Dictionary<string, string> roles = new Dictionary<string, string>();

            SqlConnection conn = DbConnect();
            conn.Open();

            SqlDataReader reader = ExecuteQuery("Select * from AspNetRoles", conn);

            while(reader.Read())
            {
                roles.Add(reader["Id"].ToString(), reader["Name"].ToString());
            }

            return roles;
        }

        //GET: Admin/Edit/{UserId}
        [HttpGet]
        [Route("admin/edit/{userId}")]
        public ActionResult Edit(string userId)
        {
            ViewBag.Roles = GetRoles();

            User usr = new User();
            List <User> users = GetUsers();

            usr = users.Find(x => x.Id == userId);

            return View(usr);
        }

        [HttpPost]
        [Route("admin/edit/{userId}")]
        public ActionResult Edit(string userId, string userRole)
        {

            string sqlCmd = "Update AspNetUserRoles Set RoleId=@role Where UserId=@userId";
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@role", userRole);
            cmd.Parameters.AddWithValue("@userId", userId);

            cmd.ExecuteNonQuery();

            return RedirectToAction("Index");
        }
    }
}