﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Inchirieri.Models;
using Microsoft.AspNet.Identity;

namespace Inchirieri.Controllers
{
    public class HomeController : Controller
    {
        public SqlConnection DbConnect()
        {
            string connectionString = @"Data Source=DESKTOP-JV89SA9\SQLEXPRESS;Initial Catalog=Inchirieri;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);

            return conn;
        }

        public SqlDataReader ExecuteQuery(string cmd, SqlConnection conn)
        {
            SqlCommand sqlCmd = new SqlCommand(cmd, conn);
            SqlDataReader reader = sqlCmd.ExecuteReader();

            return reader;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Route("orders")]
        [Authorize(Roles = "User")]
        public ActionResult Orders()
        {
            List<Order> orders = new List<Order>();

            SqlConnection conn = DbConnect();
            conn.Open();

            SqlDataReader reader = ExecuteQuery("select Rentals.order_id, Rentals.order_carid, AspNetUsers.Email, Rentals.order_startedAt, Rentals.order_endedAt, Rentals.order_totalPrice, Rentals.createdAt, Rentals.order_returnedAt from Rentals, AspNetUsers where Rentals.order_custId = AspNetUsers.Id and Rentals.order_custId = '" + User.Identity.GetUserId() +"'", conn);

            while(reader.Read())
            {
                Order ord = new Order();
                ord.OrderId = int.Parse(reader["order_id"].ToString());
                ord.CarId = int.Parse(reader["order_carId"].ToString());
                ord.UserId = reader["Email"].ToString();
                ord.StartDate = reader["order_startedAt"].ToString();
                ord.EndDate = reader["order_endedAt"].ToString();
                ord.TotalPrice = int.Parse(reader["order_totalPrice"].ToString());
                ord.CreatedAt = reader["createdAt"].ToString();

                if (DBNull.Value.Equals(reader["order_returnedAt"]))
                {
                    ord.IsReturned = false;
                    ord.ReturnedDate = "-";
                }
                else
                {
                    ord.IsReturned = true;
                    ord.ReturnedDate = reader["order_returnedAt"].ToString();
                }

                orders.Add(ord);
            }

            ViewBag.Orders = orders;

            return View();
        }

        [Route("orders/cancel/{orderId}")]
        public ActionResult CancelOrder(int orderId)
        {
            SqlConnection conn = DbConnect();
            conn.Open();

            string sqlCmd = "Update Rentals Set order_returnedAt = SYSDATETIME() where order_id =" + orderId;
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.ExecuteNonQuery();

            return RedirectToAction("Orders");
        }
    }
}